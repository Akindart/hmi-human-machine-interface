import QtQuick 2.4
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.3
import QtQuick.Controls.Private 1.0
import QtQuick.Controls.Styles 1.1

ScrollView {

    id: page

    horizontalScrollBarPolicy: Qt.ScrollBarAlwaysOff

    property bool testProperty: true

    Item{

        id: content

        signal reload

        width: page.viewport.width
        height: page.viewport.height

        Loader {
            id: mLoader
            anchors.fill: parent
        }
        Loader {
            id: mLoader2
            anchors.fill: parent
        }
        Loader {
            id: mLoader3
            anchors.fill: parent
        }


        GroupBox{

            id: optsBox
            anchors.centerIn: parent

            visible: testProperty

            ColumnLayout {

                spacing: 10

                Button{

                    id: btnMeuDia

                    style: ButtonStyle{

                        id: myBtnStyle

                        background: Rectangle {
                            implicitWidth: page.viewport.width - 30
                            implicitHeight: 25
                            radius: 0
                            color: btnMeuDia.pressed ? "gray" : "lightgray"
                        }
                        label: Label{

                            color: "black"
                            text: qsTr("Meu dia")
                            horizontalAlignment: Text.AlignHCenter

                        }

                    }

                    onClicked: tab_view.currentIndex = 2

                }

                Button{

                    id: btnAtividadesDoDia

                    style: ButtonStyle{

                        background: Rectangle {

                            implicitWidth: page.viewport.width - 30
                            implicitHeight: 25
                            radius: 0
                            color: btnAtividadesDoDia.pressed ? "gray" : "lightgray"

                        }
                        label: Label{

                            color: "black"
                            text: qsTr("Atividades Do Dia")
                            horizontalAlignment: Text.AlignHCenter

                        }

                    }

                    onClicked: {

                        optsBox.visible=false
                        mLoader3.setSource("AtividadesDia.qml")

                    }

                }

                Button{

                    id: btnMes

                    style: ButtonStyle{

                        background: Rectangle {

                            implicitWidth: page.viewport.width - 30
                            implicitHeight: 25
                            radius: 0
                            color:  btnMes.pressed ? "gray" : "lightgray"

                        }
                        label: Label{

                            color: "black"
                            text: qsTr("Mês")
                            horizontalAlignment: Text.AlignHCenter

                        }

                    }

                    onClicked: tab_view.currentIndex = 1

                }

                Button{

                    id: btnCriarAtividades

                    style: ButtonStyle{

                        background: Rectangle {

                            implicitWidth: page.viewport.width - 30
                            implicitHeight: 25
                            radius: 0
                            color:  btnCriarAtividades.pressed ? "gray" : "lightgray"

                        }
                        label: Label{

                            color: "black"
                            text: qsTr("Criar Atividades")
                            horizontalAlignment: Text.AlignHCenter

                        }

                    }

                    onClicked: {

                        optsBox.visible = false
                        mLoader.source = "CriarAtividade.qml"

                    }

                }

                Button{

                    id: btnAtividadesRegistradas

                    style: ButtonStyle{

                        background: Rectangle {

                            implicitWidth: page.viewport.width - 30
                            implicitHeight: 25
                            radius: 0
                            color: btnAtividadesRegistradas.pressed ? "gray" : "lightgray"

                        }
                        label: Label{

                            color: "black"
                            text: qsTr("Atividades registradas")
                            horizontalAlignment: Text.AlignHCenter

                        }
                    }

                    onClicked:{

                        optsBox.visible=false
                        mLoader2.setSource("ListaAtividades.qml")


                    }
                }
            }
        }


        Connections {
            target: mLoader.item
            onDestruirCriar: {
                mLoader.setSource("")
                optsBox.visible = true
            }
        }

        Connections{
            target: mLoader2.item
            onDestruirLista:{
                mLoader2.setSource("")
                optsBox.visible = true
            }
        }

        Connections{
            target: mLoader3.item
            onDestruirAtividadeDia:{
                mLoader3.setSource("")
                optsBox.visible = true
            }
        }


        Component.onDestruction: {

            mLoader.setSource("")
            mLoader2.setSource("")

        }

    }
}

