import QtQuick 2.2
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.3
import QtQuick.Controls.Private 1.0
import QtQuick.Controls.Styles 1.1

ScrollView {

    id: page

    horizontalScrollBarPolicy: Qt.ScrollBarAlwaysOff

    signal chosen(date _date)

    AtividadesDia{

        anchors.top: parent.top
        width: page.viewport.width
        height: page.viewport.height
        anchors.topMargin: 0
        Layout.alignment: Qt.AlignCenter
        id: atvD
        visible: false

        onDestruirAtividadeDia:  {

            atvD.visible=false
            calendar.visible= true


        }


    }

    Item{

        id: content

        width: page.viewport.width
        height: page.viewport.height

        Calendar {
            id: calendar
            width: content.width
            height: content.height/2
            frameVisible: true
            weekNumbersVisible: false
            selectedDate: new Date()
            focus: true

            onClicked: {

                calendar.visible=false
                atvD.visible=true

            }
        }
    }
}
