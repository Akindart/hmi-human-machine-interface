import QtQuick 2.0

Rectangle{

    id: btnBackgroundStyle

    implicitWidth: page.viewport.width - 30
    implicitHeight: 25
    border.width: control.activeFocus ? 2 : 1
    border.color: "white"
    radius: 0
    color: "lightgrey"
}
