import QtQuick 2.0

import QtQuick 2.0

import QtQuick 2.2
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.3

ScrollView {

    id: page

    horizontalScrollBarPolicy: Qt.ScrollBarAlwaysOff

    Item{

        id: content

        width: page.viewport.width
        height: page.viewport.height

        Rectangle{

            id: weather

            anchors.left:parent.left
            width: page.viewport.width
            height: page.viewport.height/3
            color: "white"
            border.color: "lightgray"
            border.width: 3

            Image{
                source: "weather.jpg"


            }


        }

        ScrollView{

            y:page.viewport.height-160

            width: page.viewport.width


            contentItem: TextArea{

                id: descricaoDoDia
                width: page.viewport.width
                height: page.viewport.height/3

                readOnly: true

                text: "Lorem ipsum..."

            }

            horizontalScrollBarPolicy: Qt.ScrollBarAlwaysOff
            verticalScrollBarPolicy:Qt.ScrollBarAlwaysOn
        }

    }

}
