import QtQuick 2.2
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.3
import QtQuick.Controls.Private 1.0
import QtQuick.Controls.Styles 1.1
import QtQuick.Dialogs 1.1


ScrollView{

    id: page
    horizontalScrollBarPolicy: Qt.ScrollBarAlwaysOff

    signal destruirLista(string msg)

    anchors.fill: parent

    VisualizarAtividade{

        id: atvV
        visible: false

        anchors.top: parent.top
        width: page.viewport.width
        height: page.viewport.height
        anchors.topMargin: 0
        Layout.alignment: Qt.AlignCenter

        onDestruirVisualizar: {

            atvV.visible = false
            listBox.visible = true

        }

    }

    GroupBox{

        anchors.top: parent.top
        width: page.viewport.width
        height: page.viewport.height
        anchors.topMargin: 0
        Layout.alignment: Qt.AlignCenter
        id: listBox

        ColumnLayout{

            anchors.topMargin: 0
            width: page.viewport.width
            height: page.viewport.height
            anchors.centerIn: parent

            spacing: 10

            Label{

                id: title

                anchors.top: parent.top
                anchors.topMargin: 20
                Layout.alignment: Qt.AlignCenter
                text: "Lista de Atividades"
                font.pointSize: 20

            }

            Button{

                id: atv1
                Layout.alignment: Qt.AlignCenter
                anchors.top: title.bottom
                anchors.topMargin: 50

                style: ButtonStyle{

                    background: Rectangle {
                        implicitWidth: page.viewport.width - 30
                        implicitHeight: 25
                        radius: 0
                        color: atv1.pressed ? "gray" : "lightgray"
                    }
                    label: Label{

                        color: "black"
                        text: "Atvididade 1"
                        horizontalAlignment: Text.AlignHCenter

                    }

                }

                onClicked: {

                    listBox.visible = false
                    atvV.visible = true
                    atvV.nomeAtividade = "Atvididade 1"

                }


            }
            Button{

                id: atv2
                Layout.alignment: Qt.AlignCenter
                anchors.top: atv1.bottom
                anchors.topMargin: 20

                style: ButtonStyle{

                    background: Rectangle {
                        implicitWidth: page.viewport.width - 30
                        implicitHeight: 25
                        radius: 0
                        color: atv2.pressed ? "gray" : "lightgray"
                    }
                    label: Label{

                        color: "black"
                        text: "Atvididade 2"
                        horizontalAlignment: Text.AlignHCenter

                    }

                }

                onClicked: {

                    listBox.visible = false
                    atvV.visible = true
                    atvV.nomeAtividade = "Atvididade 2"

                }


            }
            Button{

                id: atv3
                Layout.alignment: Qt.AlignCenter
                anchors.top: atv2.bottom
                anchors.topMargin: 20

                style: ButtonStyle{

                    background: Rectangle {
                        implicitWidth: page.viewport.width - 30
                        implicitHeight: 25
                        radius: 0
                        color: atv3.pressed ? "gray" : "lightgray"
                    }
                    label: Label{

                        color: "black"
                        text: "Atvididade 3"
                        horizontalAlignment: Text.AlignHCenter

                    }

                }

                onClicked: {

                    listBox.visible = false
                    atvV.visible = true
                    atvV.nomeAtividade = "Atvididade 3"

                }


            }
            Button{

                id: atv4
                Layout.alignment: Qt.AlignCenter
                anchors.top: atv3.bottom
                anchors.topMargin: 20

                style: ButtonStyle{

                    background: Rectangle {
                        implicitWidth: page.viewport.width - 30
                        implicitHeight: 25
                        radius: 0
                        color: atv4.pressed ? "gray" : "lightgray"
                    }
                    label: Label{

                        color: "black"
                        text: "Atvididade 4"
                        horizontalAlignment: Text.AlignHCenter

                    }

                }

                onClicked: {

                    listBox.visible = false
                    atvV.visible = true
                    atvV.nomeAtividade = "Atvididade 4"

                }

            }
            Button{

                id: atv5
                Layout.alignment: Qt.AlignCenter
                anchors.top: atv4.bottom
                anchors.topMargin: 20

                style: ButtonStyle{

                    background: Rectangle {
                        implicitWidth: page.viewport.width - 30
                        implicitHeight: 25
                        radius: 0
                        color: atv5.pressed ? "gray" : "lightgray"
                    }
                    label: Label{

                        color: "black"
                        text: "Atvididade 5"
                        horizontalAlignment: Text.AlignHCenter

                    }

                }

                onClicked: {

                    listBox.visible = false
                    atvV.visible = true
                    atvV.nomeAtividade = "Atvididade 5"

                }

            }

            Button{

                id: btnVoltar

                anchors.bottom: parent.bottom
                anchors.bottomMargin: 30

                Layout.alignment: Qt.AlignCenter


                style: ButtonStyle{

                    background: Rectangle {

                        implicitWidth: 100
                        implicitHeight: 25
                        radius: 5
                        color: btnVoltar.pressed ? "gray" : "lightgray"

                    }
                    label: Label{

                        color: "black"
                        text: qsTr("Voltar")
                        horizontalAlignment: Text.AlignHCenter

                    }

                }

                onClicked: {

                    destruirLista("Lista de atividades destruida")


                }

            }

        }

    }

}




