import QtQuick 2.2
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.3
import QtQuick.Controls.Private 1.0
import QtQuick.Controls.Styles 1.1
import QtQuick.Dialogs 1.1


ScrollView{

    id: page
    horizontalScrollBarPolicy: Qt.ScrollBarAlwaysOff

    signal destruirDefinirAtividade()

    property string atvNome: "test"

    anchors.fill: parent

    GroupBox{

        anchors.top: parent.top
        width: page.viewport.width
        height: page.viewport.height
        anchors.topMargin: 0
        Layout.alignment: Qt.AlignCenter
        id: listBox

        ColumnLayout{

            id:titleLayout

            anchors.top: parent.top
            width: page.viewport.width
            anchors.topMargin: 0

            spacing: 10

            RowLayout{

                Layout.alignment: Qt.AlignCenter

                Label{

                    id: semana
                    anchors.top: parent.top
                    anchors.topMargin: 0

                    text: "Semana X"
                    font.pointSize: 30

                }

                ColumnLayout{

                    anchors.left: semana.right
                    anchors.leftMargin: 10

                    Label{

                        id: mes
                        anchors.top: parent.top
                        anchors.topMargin: 0
                        Layout.alignment: Qt.AlignCenter
                        text: "Mes Y"
                        font.pointSize: 20
                    }

                }

            }

        }

        RowLayout{

            id: horariosLayout

            anchors.top: titleLayout.bottom
            anchors.topMargin: 20

            ColumnLayout{

                anchors.top: parent.top
                Column{
                    Repeater{

                        model: 10

                        Rectangle{

                            color: "transparent"
                            width: 100; height: 40

                            Label{

                                anchors.centerIn: parent
                                text: "Hora " + (index+1)
                                color: "black"

                            }

                        }

                    }

                }

            }

            ScrollView{

                horizontalScrollBarPolicy: Qt.ScrollBarAlwaysOn
                verticalScrollBarPolicy: Qt.ScrollBarAlwaysOn

                implicitWidth: (90*3)
                implicitHeight: (40*10)

                contentItem: Column{

                    Repeater{

                        model: 10

                        Row{

                            Repeater{

                                model: 8

                                Rectangle{

                                    color: "lightgray"
                                    width: 90; height: 40
                                    border.width: 1
                                    border.color: "black"

                                    Label{

                                        anchors.fill: parent
                                        anchors.centerIn: parent
                                        text: "Horario vago"

                                        MouseArea{

                                            anchors.fill: parent

                                            onClicked: {

                                                parent.parent.color= "gray"
                                                parent.text=atvNome

                                            }

                                        }


                                    }


                                }

                            }

                        }

                    }

                }

            }

        }

        RowLayout{

            id: finalBtnsLayout

            anchors.top: horariosLayout.bottom
            anchors.topMargin: 30
            implicitWidth: page.viewport.width


            Button{

                id: btnVoltar

                anchors.left: finalBtnsLayout.left
                anchors.leftMargin: 25

                style: ButtonStyle{

                    background: Rectangle {

                        implicitWidth: 100
                        implicitHeight: 25
                        radius: 5
                        color: btnVoltar.pressed ? "gray" : "lightgray"

                    }
                    label: Label{

                        color: "black"
                        text: qsTr("Voltar")
                        horizontalAlignment: Text.AlignHCenter

                    }

                }


                onClicked: {

                    atividadeCriada.text = atvNome + " fnão foi alocada"
                    atividadeCriada.open()
                    destruirDefinirAtividade()

                }

            }

            Button{

                id: btnSalvar

                anchors.left: btnVoltar.right
                anchors.leftMargin: 125

                style: ButtonStyle{

                    background: Rectangle {

                        implicitWidth: 100
                        implicitHeight: 25
                        radius: 5
                        color: btnSalvar.pressed ? "gray" : "lightgray"

                    }
                    label: Label{

                        color: "black"
                        text: qsTr("Salvar")
                        horizontalAlignment: Text.AlignHCenter

                    }

                }

                onClicked: {

                    atividadeCriada.text = atvNome + " foi alocada com sucesso"
                    atividadeCriada.open()
                    destruirDefinirAtividade()

                }

            }

        }

    }

}




