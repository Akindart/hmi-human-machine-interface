import QtQuick 2.2
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.3
import QtQuick.Controls.Private 1.0
import QtQuick.Controls.Styles 1.1
import QtQuick.Dialogs 1.1


ScrollView{

    id: page
    horizontalScrollBarPolicy: Qt.ScrollBarAlwaysOff

    signal destruirAtividadeDia()
    signal changeName()

    property string atvNome: "Atividades Sugeridas"



    anchors.fill: parent

    AtividadesSugeridas{

        anchors.top: parent.top
        width: page.viewport.width
        height: page.viewport.height
        anchors.topMargin: 0
        Layout.alignment: Qt.AlignCenter
        id: atvS
        visible: false

        onDestruirListaAtividades: {

            listBox.visible= true
            atvS.visible=false

        }

    }


    GroupBox{

        anchors.top: parent.top
        width: page.viewport.width
        height: page.viewport.height
        anchors.topMargin: 0
        Layout.alignment: Qt.AlignCenter
        id: listBox

        ColumnLayout{

            id:titleLayout

            anchors.top: parent.top
            width: page.viewport.width
            anchors.topMargin: 0

            spacing: 10

            RowLayout{

                Layout.alignment: Qt.AlignLeft

                Label{

                    id: dia
                    anchors.left: parent.left
                    anchors.topMargin: 0

                    text: "Dia X"
                    font.pointSize: 40

                }

                ColumnLayout{

                    id: _2layout

                    anchors.left: dia.right
                    anchors.leftMargin: 10

                    Label{

                        id: mes
                        anchors.top: parent.top
                        anchors.topMargin: 0
                        Layout.alignment: Qt.AlignCenter
                        text: "Mes Y"
                        font.pointSize: 20
                    }

                }

                Rectangle{

                    id: atvdFill
                    anchors.left: _2layout.right
                    anchors.leftMargin: 80
                    color: "lightgray"
                    implicitWidth: 48
                    implicitHeight: 48

                    MouseArea{

                        anchors.fill: parent

                        onPressed: {

                            parent.color= "gray"

                        }
                        onReleased: {

                            parent.color= "lightgray"

                        }
                        onClicked: {

                            rect2.color="gray"
                            rect2.border.color="lightgray"
                            label2.text="   Atividades Sugeridas"
                            rect3.color="gray"
                            rect3.border.color="lightgray"
                            label3.text="   Atividades Sugeridas"
                            rect7.color="gray"
                            rect7.border.color="lightgray"
                            label7.text="   Atividades Sugeridas"
                            rect9.color="gray"
                            rect9.border.color="lightgray"
                            label9.text="   Atividades Sugeridas"


                        }

                    }

                    Image{

                        anchors.fill: parent
                        source: "qrc:///write_file.png"

                    }



                }

            }

        }

        RowLayout{

            id: horariosLayout

            anchors.top: titleLayout.bottom
            anchors.topMargin: 20

            ColumnLayout{

                anchors.top: parent.top
                Column{
                    Repeater{

                        model: 10

                        Rectangle{

                            color: "transparent"
                            width: 100; height: 40

                            Label{

                                anchors.centerIn: parent
                                text: "Hora " + (index+1)
                                color: "black"

                            }

                        }

                    }

                }

            }

            ScrollView{

                horizontalScrollBarPolicy: Qt.ScrollBarAlwaysOff
                verticalScrollBarPolicy: Qt.ScrollBarAlwaysOff

                implicitWidth: (90*3)
                implicitHeight: (40*10)

                contentItem: Column{

                    Rectangle{

                        id: rect1
                        color: "lightgray"
                        width: 230; height: 40
                        border.width: 1
                        border.color: "black"
                        radius: 5

                        Label{

                            id: label1
                            anchors.fill: parent
                            anchors.centerIn: parent
                            text: "Horario vago"

                            MouseArea{

                                anchors.fill: parent

                                onClicked: {




                                }

                            }

                        }

                    }
                    Rectangle{

                        id: rect2
                        color: "lightgray"
                        width: 230; height: 40
                        border.width: 1
                        border.color: "black"
                        radius: 5

                        Label{

                            id: label2
                            anchors.fill: parent
                            anchors.centerIn: parent
                            text: "Horario vago"

                            MouseArea{

                                anchors.fill: parent

                                onClicked: {
                                    atvS.visible=true
                                    listBox.visible=false



                                }

                            }

                        }

                    }
                    Rectangle{

                        id: rect3
                        color: "lightgray"
                        width: 230; height: 40
                        border.width: 1
                        border.color: "black"
                        radius: 5

                        Label{

                            id: label3
                            anchors.fill: parent
                            anchors.centerIn: parent
                            text: "Horario vago"

                            MouseArea{

                                anchors.fill: parent

                                onClicked: {
                                    atvS.visible=true
                                    listBox.visible=false



                                }

                            }

                        }

                    }
                    Rectangle{

                        id: rect4
                        color: "lightgray"
                        width: 230; height: 40
                        border.width: 1
                        border.color: "black"
                        radius: 5

                        Label{

                            id: label4
                            anchors.fill: parent
                            anchors.centerIn: parent
                            text: "Horario vago"

                            MouseArea{

                                anchors.fill: parent

                                onClicked: {



                                }

                            }

                        }

                    }
                    Rectangle{

                        id: rect5
                        color: "lightgray"
                        width: 230; height: 40
                        border.width: 1
                        border.color: "black"
                        radius: 5

                        Label{

                            id: label5
                            anchors.fill: parent
                            anchors.centerIn: parent
                            text: "Horario vago"

                            MouseArea{

                                anchors.fill: parent

                                onClicked: {





                                }

                            }

                        }

                    }
                    Rectangle{

                        id: rect6
                        color: "lightgray"
                        width: 230; height: 40
                        border.width: 1
                        border.color: "black"
                        radius: 5

                        Label{

                            id: label6
                            anchors.fill: parent
                            anchors.centerIn: parent
                            text: "Horario vago"

                            MouseArea{

                                anchors.fill: parent

                                onClicked: {




                                }

                            }

                        }

                    }
                    Rectangle{

                        id: rect7
                        color: "lightgray"
                        width: 230; height: 40
                        border.width: 1
                        border.color: "black"
                        radius: 5

                        Label{

                            id: label7
                            anchors.fill: parent
                            anchors.centerIn: parent
                            text: "Horario vago"

                            MouseArea{

                                anchors.fill: parent

                                onClicked: {
                                    atvS.visible=true
                                    listBox.visible=false



                                }

                            }

                        }

                    }
                    Rectangle{

                        id: rect8
                        color: "lightgray"
                        width: 230; height: 40
                        border.width: 1
                        border.color: "black"
                        radius: 5

                        Label{

                            id: label8
                            anchors.fill: parent
                            anchors.centerIn: parent
                            text: "Horario vago"

                            MouseArea{

                                anchors.fill: parent

                                onClicked: {




                                }

                            }

                        }

                    }
                    Rectangle{

                        id: rect9
                        color: "lightgray"
                        width: 230; height: 40
                        border.width: 1
                        border.color: "black"
                        radius: 5

                        Label{

                            id: label9
                            anchors.fill: parent
                            anchors.centerIn: parent
                            text: "Horario vago"

                            MouseArea{

                                anchors.fill: parent

                                onClicked: {
                                    atvS.visible=true
                                    listBox.visible=false

                                }

                            }

                        }

                    }
                    Rectangle{

                        id: rect10
                        color: "lightgray"
                        width: 230; height: 40
                        border.width: 1
                        border.color: "black"
                        radius: 5

                        Label{

                            id: label10
                            anchors.fill: parent
                            anchors.centerIn: parent
                            text: "Horario vago"

                            MouseArea{

                                anchors.fill: parent

                                onClicked: {




                                }

                            }

                        }

                    }

                }
            }

        }

        RowLayout{

            id: finalBtnsLayout

            anchors.top: horariosLayout.bottom
            anchors.topMargin: 30
            implicitWidth: page.viewport.width


            Button{

                id: btnVoltar

                anchors.left: finalBtnsLayout.left
                anchors.leftMargin: 25

                style: ButtonStyle{

                    background: Rectangle {

                        implicitWidth: 100
                        implicitHeight: 25
                        radius: 5
                        color: btnVoltar.pressed ? "gray" : "lightgray"

                    }
                    label: Label{

                        color: "black"
                        text: qsTr("Voltar")
                        horizontalAlignment: Text.AlignHCenter

                    }

                }


                onClicked: {

                    destruirAtividadeDia()

                }

            }

        }

    }

}




