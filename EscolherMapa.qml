import QtQuick 2.2
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.3
import QtQuick.Controls.Private 1.0
import QtQuick.Controls.Styles 1.1
import QtQuick.Dialogs 1.1


ScrollView {


    signal destruirMapa(string msg)

    id: page
    anchors.fill: parent
    visible:true

    horizontalScrollBarPolicy: Qt.ScrollBarAlwaysOff

    Item{

        id: content

        width: page.viewport.width
        height: page.viewport.height
        visible:true

        ColumnLayout{

            id: mapLayout

            Rectangle{

                id: weather

                anchors.left:parent.left
                width: page.viewport.width
                height: page.viewport.height/3
                color: "white"
                border.color: "lightgray"
                border.width: 3
                visible:true

                Label{

                    anchors.centerIn: parent
                    text: "Mapa"
                    color: "black"

                }


            }

            Button{

                id: btnSalvarMapa

                anchors.top: parent.bottom

                Layout.alignment: Qt.AlignCenter
                visible:true

                style: ButtonStyle{

                    background: Rectangle {

                        implicitWidth: 100
                        implicitHeight: 25
                        radius: 5
                        color: btnSalvar.pressed ? "gray" : "lightgray"

                    }
                    label: Label{

                        color: "black"
                        text: qsTr("Salvar")
                        horizontalAlignment: Text.AlignHCenter

                    }

                }

            }

        }

    }

}

