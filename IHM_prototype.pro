TEMPLATE = app
TARGET = IHM_prototype

QT += qml quick widgets

SOURCES += main.cpp

RESOURCES += qml.qrc

ICON += map_icon.png

OTHER_FILES += \
    main.qml \
    MonthPage.qml


# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH = qml/MonthPage.qml

# Default rules for deployment.
include(deployment.pri)

HEADERS += \
    escolhermapa.h
