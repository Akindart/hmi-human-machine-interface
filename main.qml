import QtQuick 2.4
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.3
import QtQuick.Controls.Styles 1.3
import QtQuick.Window 2.2
import QtQuick.Dialogs 1.2


ApplicationWindow {

    id: app_window
    visible: true
    title: qsTr("Agenda")

    ColorDialog {
        id: colorDialog
        title: "Please choose a color"
        color: "#223266"
        onAccepted: {
            console.log("You chose: " + colorDialog.color)
            //Qt.quit()
        }
        onRejected: {
            console.log("Canceled")
            //Qt.quit()
        }
        //Component.onCompleted: visible = true
    }

    MessageDialog {
        id: atividadeCriada
        title: "Atividade"
        text: "Atividade Criada com sucesso"
        onAccepted: {
            console.log("And of course you could only agree.")
            //Qt.quit()
        }

    }

    width: 399
    height: 600


    TabView {
        id: tab_view
        anchors.fill: parent
        anchors.margins: 4

        Tab {

            id: opts
            title: "Opts"
            source: "Opts.qml"

        }
        Tab {

            id: mes
            title: "Mês"
            source: "MonthPage.qml"


        }
        Tab {

            title: "Hoje"
            id: hj
            source: "MeuDia.qml"


        }

        style: TabViewStyle {
            frameOverlap: 1
            tab: Rectangle {
                color: styleData.selected ? "lightgray" :"white"
                border.color:   "gray"
                implicitWidth: Math.max(app_window.width/3, text.width+4)
                implicitHeight: 35
                border.width: 5

                radius: 0
                Text {
                    id: text
                    anchors.centerIn: parent
                    text: styleData.title
                    color: "black"
                    horizontalAlignment: Text.AlignHCenter
                }


            }
            frame: Rectangle  { color: "#bbb4b4"    }
        }


        onCurrentIndexChanged: {

            opts.setSource("")
            opts.setSource("Opts.qml")

        }

        Connections {
            target: mes.item
            onChosen: {

                console.log(_date)
                tab_view.currentIndex = 0

            }

        }

    }

}





















