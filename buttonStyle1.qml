import QtQuick 2.2
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.3
import QtQuick.Controls.Private 1.0
import QtQuick.Controls.Styles 1.1
import QtQuick.Dialogs 1.1

Item{

    ButtonStyle{

        property Button btn
        property string nameBtn

        background: Rectangle {

            implicitWidth: 100
            implicitHeight: 25
            radius: 5
            color: btn.pressed ? "gray" : "lightgray"

        }
        label: Label{

            color: "black"
            text: nameBtn
            horizontalAlignment: Text.AlignHCenter

        }

    }


}

