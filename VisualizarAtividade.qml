import QtQuick 2.2
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.3
import QtQuick.Controls.Private 1.0
import QtQuick.Controls.Styles 1.1
import QtQuick.Dialogs 1.1


ScrollView{

    id: page
    horizontalScrollBarPolicy: Qt.ScrollBarAlwaysOff

    signal destruirVisualizar()

    anchors.fill: parent

    property string nomeAtividade: "Entre com o nome da atividade"

    CriarAtividade{

        id: atvC
        visible: false

        endDialog: "\" modificada com sucesso. (mentira, eu nao armazenei nada LoL =D)"
        nomeFunc: "Modificar atividade"

        anchors.top: parent.top
        width: page.viewport.width
        height: page.viewport.height
        anchors.topMargin: 0
        Layout.alignment: Qt.AlignCenter

        onDestruirCriar: {

            atvC.visible = false
            atvdNome.visible = true

        }

    }

    DefinirData{

        id: dfnrA
        visible: false


        anchors.top: parent.top
        width: page.viewport.width
        height: page.viewport.height
        anchors.topMargin: 0
        Layout.alignment: Qt.AlignCenter

        onDestruirDefinirAtividade: {

            dfnrA.visible = false
            atvdNome.visible = true

        }


    }

    ColumnLayout{

        anchors.topMargin: 0
        width: page.viewport.width
        height: page.viewport.height

        GroupBox{

            id: atvdNome

            Layout.alignment: Qt.AlignTop

            ColumnLayout{

                id: columnL1

                width: page.viewport.width
                height: funcLabel.height + nomeAtvd.height + divisor.height + 20

                Rectangle{

                    id: funcLabel

                    Layout.alignment: Qt.AlignTop

                    implicitWidth:  page.viewport.width-15
                    implicitHeight: page.viewport.height/5
                    color: "lightgray"
                    radius:  5
                    border.color: "black"
                    border.width: 5

                    Label{

                        anchors.centerIn: parent
                        text: "Visualizar Atividade"
                        color: "black"
                        font.pointSize: 20

                    }

                }

                TextField{

                    id: nomeAtvd

                    anchors.top: funcLabel.bottom
                    anchors.topMargin: 10

                    text: nomeAtividade
                    readOnly: true
                    implicitWidth:  funcLabel.implicitWidth
                    implicitHeight: 30
                    font.pointSize: 10

                }


                Rectangle{

                    id: divisor

                    anchors.top: nomeAtvd.bottom
                    anchors.topMargin: 10

                    Layout.alignment: Qt.AlignTop

                    implicitWidth:  funcLabel.implicitWidth
                    implicitHeight: 5

                    color: "black"

                }

                ColumnLayout{

                    spacing: 10

                    width: page.viewport.width
                    id: infos_layout
                    anchors.top: columnL1.bottom
                    anchors.topMargin: 10

                    Label{

                        id: infos
                        text: "Informações"

                        font.pointSize: 12
                        color: "black"

                    }

                    RowLayout{

                        spacing: 5

                        id: duracao_layout

                        anchors.top: infos.bottom
                        anchors.topMargin: 10

                        Label{

                            id: duracao
                            text: "Duração"
                            font.pointSize: 12
                            color: "black"

                        }

                        ComboBox{

                            id: horas

                            anchors.left: duracao.right
                            anchors.leftMargin: 20

                            currentIndex: 0
                            model: [" ", "1 hora", "2 hors", "3 horas", "4 horas", "5 horas", "6 horas",
                                "7 horas", "8 horas", "9 horas", "10 horas", "11 horas", "12 horas",
                                "13 horas", "14 horas", "15 horas", "16 horas", "17 horas", "18 horas",
                                "19 horas", "20 horas", "21 horas", "22 horas", "23 horas", "24 horas"]
                            enabled: false

                        }
                        ComboBox{

                            id: mins

                            anchors.left: horas.right
                            anchors.leftMargin: 20

                            currentIndex: 0
                            model: ["0 minutos", "5 minutos", "10minutos", "15 minutos", "20 minutos", "25 minutos",
                                "35 minutos", "40 minutos", "45 minutos", "50 minutos", "55 minutos"]
                            enabled: false

                        }

                    }
                    RowLayout{

                        spacing: 5

                        id: local_layout

                        anchors.top: duracao_layout.bottom
                        anchors.topMargin: 10

                        ColumnLayout{

                            id: localLayout

                            Label{

                                id: local
                                text: "Local"
                                font.pointSize: 12
                                color: "black"

                            }
                            Label{

                                id: rua
                                text: "Rua: Lorem, n XXXX"
                                font.pointSize: 10
                                color: "black"

                            }
                            Label{

                                id: cidade
                                text: "Cidade: Ipsum, Bairro alguma coisa, estado ET"
                                font.pointSize: 10
                                color: "black"

                            }


                        }

                        Button{

                            id: btnLocal
                            anchors.right: localLayout.right
                            anchors.leftMargin: 10
                            Layout.alignment: Qt.AlignCenter

                            style: ButtonStyle{

                                background: Rectangle {

                                    implicitWidth: 32
                                    implicitHeight: 32
                                    radius: 0
                                    color: "transparent"
                                    Image{
                                        source: "qrc:///map_icon.png"
                                        anchors.fill: parent

                                    }

                                }

                            }

                            onClicked: {

                                infos_layout.visible = false
                                mapLayout.visible = true
                            }

                        }

                    }
                    RowLayout{

                        spacing: 5

                        id: cor_layout

                        anchors.top: local_layout.bottom
                        anchors.topMargin: 10
                        Label{

                            id: cor
                            text: "Cor Padrão"
                            font.pointSize: 12
                            color: "black"

                        }


                        Rectangle {

                            id: colorchoosed
                            anchors.left: cor.right
                            anchors.leftMargin: 10
                            Layout.alignment: Qt.AlignCenter

                            implicitWidth: 32
                            implicitHeight: 32
                            radius: 0
                            color: colorDialog.color

                        }

                    }

                    Label{

                        anchors.top: cor_layout.bottom
                        anchors.topMargin: 10

                        id: descricao
                        text: "Adicionar Descricao"
                        font.pointSize: 12
                        color: "black"

                    }

                    ScrollView{

                        id:text_descricao

                        anchors.top: descricao.bottom
                        anchors.topMargin: 10

                        implicitWidth:  page.viewport.width-15
                        implicitHeight: page.viewport.height/5

                        contentItem: TextArea{

                            id: descricaoDoDia
                            width: page.viewport.width
                            height: page.viewport.height/3

                            readOnly: true

                            text: "Lorem ipsum..."

                        }

                        horizontalScrollBarPolicy: Qt.ScrollBarAlwaysOff
                        verticalScrollBarPolicy:Qt.ScrollBarAlwaysOn
                    }

                    RowLayout{

                        id: finalBtnsLayout

                        anchors.top: text_descricao.bottom
                        anchors.topMargin: 15
                        implicitWidth: page.viewport.width


                        Button{

                            id: btnData

                            anchors.left: finalBtnsLayout.left
                            anchors.leftMargin: 5


                            style: ButtonStyle{

                                background: Rectangle {

                                    implicitWidth: 100
                                    implicitHeight: 20
                                    radius: 5
                                    color: btnData.pressed ? "gray" : "lightgray"

                                }
                                label: Label{

                                    color: "black"
                                    text: qsTr("Definir Data")
                                    horizontalAlignment: Text.AlignHCenter

                                }

                            }

                            onClicked: {

                                atvdNome.visible = false
                                dfnrA.atvNome=nomeAtividade
                                dfnrA.visible=true

                            }

                        }

                        Button{

                            id: btnVoltar

                            anchors.left: btnData.right
                            anchors.leftMargin: 35

                            style: ButtonStyle{

                                background: Rectangle {

                                    implicitWidth: 100
                                    implicitHeight: 25
                                    radius: 5
                                    color: btnVoltar.pressed ? "gray" : "lightgray"

                                }
                                label: Label{

                                    color: "black"
                                    text: qsTr("Voltar")
                                    horizontalAlignment: Text.AlignHCenter

                                }

                            }

                            onClicked: {

                                destruirVisualizar()

                            }

                        }

                        Button{

                            id: btnEditar

                            anchors.left: btnVoltar.right
                            anchors.leftMargin: 35

                            style: ButtonStyle{

                                background: Rectangle {

                                    implicitWidth: 100
                                    implicitHeight: 25
                                    radius: 5
                                    color: btnEditar.pressed ? "gray" : "lightgray"

                                }
                                label: Label{

                                    color: "black"
                                    text: qsTr("Editar")
                                    horizontalAlignment: Text.AlignHCenter

                                }

                            }

                            onClicked: {

                                atvdNome.visible = false
                                atvC.nomeFunc = "Modificar Atividade"
                                atvC.visible=true
                                atvC.nomeAtividade=nomeAtividade
                            }

                        }


                    }




                }

            }

            ColumnLayout{

                id: mapLayout
                visible:false

                spacing: 10

                width: page.viewport.width
                anchors.top: columnL1.bottom
                anchors.topMargin: 10
                height: 250

                Rectangle{

                    id: weather

                    anchors.left:parent.left
                    implicitWidth:  page.viewport.width-15
                    implicitHeight: page.viewport.height/4

                    Image{
                        source: "qrc:///map.jpg"
                        anchors.fill: parent

                    }



                }

                Button{

                    id: btnVoltarMapa

                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: 40

                    Layout.alignment: Qt.AlignCenter


                    style: ButtonStyle{

                        background: Rectangle {

                            implicitWidth: 100
                            implicitHeight: 25
                            radius: 5
                            color: btnVoltarMapa.pressed ? "gray" : "lightgray"

                        }
                        label: Label{

                            color: "black"
                            text: qsTr("Voltar")
                            horizontalAlignment: Text.AlignHCenter

                        }

                    }

                    onClicked: {

                        mapLayout.visible = false;
                        infos_layout.visible = true;


                    }

                }

            }

        }

    }

}



